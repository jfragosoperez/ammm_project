Analyzing and solving this paper

"An Efficient Mixed-Integer Linear Programming Scheduling Framework for Addressing Sequence-Dependent Setup Issues in Batch Plants"
@authors: Georgios M. Kopanos, José Miguel Laínez, and Luis Puigjaner

using linear programming and a couple of metaheuristics algorithms (GRASP and BRKGA)