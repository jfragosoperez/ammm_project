/*********************************************
 * OPL 12.5.1.0 Model
 * Author: eduard.sanou
 * Creation Date: 29/09/2014 at 10:12:51
 *********************************************/

execute {
  //writeln("Executing...");
};


/**
*
* In our case M = greater completion time + greater sd + greater pt + greater su.
* We could try to estimate how big can be the grater completion time, but for now
* we choose an M = 1000.
* 
*/
int M = 1000;

int n_units=...;
int n_orders=...;
int n_orders_processing=...;
int n_days=...;

int invalid=...;

range I=1..n_orders_processing;
range I_read=1..n_orders;
range J=1..n_units;
range D=1..n_days;

int D_i[i in I_read]=...;
float pt_ij[i in I_read, j in J]=...;
float su_ij[j in J]=...;
float sd_ii1j[i in I_read, i1 in I_read]=...;

dvar float C_i[i in I];
dvar float+ E_i[i in I];
dvar float+ T_i[i in I];

dvar int Pos_ii1j[i in I, i1 in I, j in J];

dvar boolean Y_ij[i in I, j in J];
dvar boolean X_ii1j[i in I, i1 in I, j in J];
dvar boolean Seq_ii1j[i in I, i1 in I, j in J];

int alfa=1;
int beta=5;

// Objective
maximize sum(i in I) C_i[i];
//minimize sum(i in I) (alfa * E_i[i] + beta * T_i[i]);
//minimize sum(i in I) E_i[i];

subject to {
  
  // TODO
  /**
  * Modifiqueu el constraint (1) de manera que sigui un ==. Això és necessari perquè en el case study que considereu 
  * s'han d'executar totes les tasques.
  *
  **MODIFIED BY THE EXTREME SARDANA PARTY :)
  */
  // Allocation Constrains
  // (1)
  
  forall(i in I)
    sum(j in J : pt_ij[i,j] != invalid) Y_ij[i,j] == 1;
  
  // Timing Constraints
  // (2)
  
  forall(i in I)
    C_i[i] >= sum(j in J : pt_ij[i,j] != invalid)
    	((pt_ij[i,j] + su_ij[j]) * Y_ij[i,j]);
    	//(max(ru_j[j], ro_i[i]) + pt_ij[i,j] + su_ij[i,j]) * Y_ij[i,j];    	
  	
  // Sequencing-Timing Constraints
  // (3)
  
  forall(i in I, i1 in I, j in J :
  		(i != i1) && (pt_ij[i,j] != invalid) && (pt_ij[i1,j] != invalid))
  	C_i[i] + sd_ii1j[i, i1] * Seq_ii1j[i, i1, j] <= 
  		C_i[i1] - pt_ij[i1, j] - su_ij[j] + M * (1 - X_ii1j[i, i1, j]);
  	// Motivating example
  	//C_i[i] <=
  	//	C_i[i1] - pt_ij[i1, j] - su_ij[j] + M * (1 - X_ii1j[i, i1, j]);

  
  // TODO 
  /** Modifiqueu el constraint (4) [el que jo comentava que era redundant]. Aquí s'ha colat i el constraint que es 
  * necessita és Xi,i1,j + Xi1,i,j + 1 >= Yi,j + Yi1,j 
  * El que necessitem imposar és que si tant i com i1 s'executen en j, aleshores o bé i 
  * s'executa abans de i1 o bé al revés.
  *
  **MODIFIED BY THE EXTREME SARDANA PARTY :)
  */
  // Sequencing
  // (4)
  
  forall(i in I, i1 in I, j in J : 
  		(i != i1) && (pt_ij[i,j] != invalid) && (pt_ij[i1,j] != invalid))
  	//X_ii1j[i, i1, j] + X_ii1j[i1, i, j] <=  Y_ij[i, j] + Y_ij[i1, j] + 1;
  	1 + X_ii1j[i, i1, j] + X_ii1j[i1, i, j] >=  Y_ij[i, j] + Y_ij[i1, j];	
  		
  // (5)
  forall(i in I, i1 in I, j in J : 
  		(i != i1) && (pt_ij[i,j] != invalid) && (pt_ij[i1,j] != invalid))
	2 * (X_ii1j[i, i1, j] + X_ii1j[i1, i, j]) <= Y_ij[i, j] + Y_ij[i1, j];
  	
  // (6)
  forall(i in I, i1 in I, j in J : 
  		(i != i1) && (pt_ij[i,j] != invalid) && (pt_ij[i1,j] != invalid))
  	Pos_ii1j[i, i1, j] == (sum(i11 in I : (i11 != i) && (i11 != i1))
  		((X_ii1j[i, i11, j] - X_ii1j[i1, i11, j]))) + M * (1 - X_ii1j[i, i1, j]);

  // (7)
  forall(i in I, i1 in I, j in J : 
  		(i != i1) && (pt_ij[i,j] != invalid) && (pt_ij[i1,j] != invalid))
  	Pos_ii1j[i, i1, j] + Seq_ii1j[i, i1, j] >= 1;
  	
  // (8)
  forall(i in I)
    sum(i1 in I : i1 != i)
      sum(j in J : (pt_ij[i,j] != invalid) && (pt_ij[i1,j] != invalid))
        Seq_ii1j[i, i1, j] <= 1;
  
  // Earliness
  // (9)
  forall(i in I)
    //E_i[i] >= D_i[i] - C_i[i];
    E_i[i] == D_i[i] - C_i[i];
    
  // Tardiness
  // (9)
  forall(i in I)
    //0 >= C_i[i] - D_i[i];
    T_i[i] >= C_i[i] - D_i[i];
   
  //E_i and T_i must be greater or equal than 0
    
  forall(i in I)
    T_i[i] == 0;

  //forall(i in I)
  //  E_i[i] >= 0;

  //TODO 
  /** 
  * Fent aquests canvis triga moltíssim i tampoc dóna el resultat correcte.
  * El que us suggereixo és que agafeu el motivating example (només les 15 primeres tasques 
  * i sense sequence-dependent setup times) i a veure si podeu reproduir el resultat que indiquen.
  * A continuació, afegiu el 9 com a transició entre 12 i 10 i hauríeu d'aconseguir el resultat que indiquen. */

}