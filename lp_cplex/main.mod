/*********************************************
 * OPL 12.5.1.0 Model
 * Author: eduard.sanou
 * Creation Date: 29/09/2014 at 11:00:15
 *********************************************/

main {

  var bench = false;
  var n_ord = 16;

  var src = new IloOplModelSource("scheduling.mod");
  var def = new IloOplModelDefinition(src);
  
  if (bench) {
    for (var i = 1; i <= 25; i++) {
  	  var cplex = new IloCplex();
  	  var model = new IloOplModel(def, cplex);
	  var data = new IloOplDataSource("input_base.dat");  
	  model.addDataSource(data);
	  
	  var data_mod = new IloOplDataElements();
	  data_mod.n_orders_processing = i;
	  model.addDataSource(data_mod);
	  
	  model.generate();
	  
	  //cplex.epgap = 0.01;
	  //cplex.epgap = 0.01;
	  // default is 1e-04
	  
	  var before = new Date();
	  var temp = before.getTime();
	  
	  var solved = cplex.solve();
	  
	  var after = new Date();
	  var sol_time = after.getTime()-temp;
	  
	  writeln(i, " ", cplex.getObjValue(), " ", sol_time)
	  
	  model.end();
	  data.end();
	  cplex.end();
    }
  } else {
      var cplex = new IloCplex();
	  var model = new IloOplModel(def, cplex);
	  var data = new IloOplDataSource("input_base.dat");  
	  model.addDataSource(data);
	  
	  var data_mod = new IloOplDataElements();
	  data_mod.n_orders_processing = n_ord;
	  model.addDataSource(data_mod);
	  
	  model.generate();
	  
	  var before = new Date();
	  var temp = before.getTime();
	  
	  var solved = cplex.solve();
	  
	  var after = new Date();
	  var sol_time = after.getTime()-temp;
	  
	  writeln("solving time = ", sol_time);
  
	  if (solved) {
	    writeln("Solved!");
	    writeln("OF: ", cplex.getObjValue());
	    writeln("batch\tUnit\tTsi\tCi\tEi\tTi");
		for (var i = 1; i <= model.n_orders_processing; i++) {
		  var u = 0;
		  for (var j = 1; j <= model.n_units; j++) {
	            if (model.Y_ij[i][j] == 1) {
	              u = j;
	            }
		  }
		  writeln("i" + i + "\t\t" + u + "\t\t" + "???" + "\t" + model.C_i[i] + "\t" + model.E_i[i], "\t", model.T_i[i]);
		}
		writeln()
		
		/*
		for (var j = 1; j <= model.n_units; j++) {
	            writeln("Pos_ii1 for unit " + j);
	            for (var i = 1; i <= model.n_orders_processing; i++) {
	                for (var i1 = 1; i1 <= model.n_orders_processing; i1++) {
	                    if (model.Pos_ii1j[i][i1][j] < 0) {
	                        write("X")
	                    } else if (model.Pos_ii1j[i][i1][j] == 0) {
	                        write("0")
	                    } else {
	                        write("M")
	                    }
	                    write(" ");
	                }
	                writeln();
	            }
	            writeln();
	        }
	        for (var j = 1; j <= model.n_units; j++) {
	            writeln("Seq_ii1 for unit " + j);
	            for (var i = 1; i <= model.n_orders_processing; i++) {
	                for (var i1 = 1; i1 <= model.n_orders_processing; i1++) {
	                    write(model.Seq_ii1j[i][i1][j] + " ");
	                }
	                writeln();
	            }
	            writeln();
	        }
		for (var j = 1; j <= model.n_units; j++) {
	            writeln("X_ii1 for unit " + j);
	            for (var i = 1; i <= model.n_orders_processing; i++) {
	                for (var i1 = 1; i1 <= model.n_orders_processing; i1++) {
	                    write(model.X_ii1j[i][i1][j] + " ");
	                }
	                writeln();
	            }
	            writeln();
	        }
	    */
	  
	  } else {
	    writeln("Not solution found");
	  }
	  model.end();
	  data.end();
	  cplex.end();
  }
  def.end();
  src.end();
};      
