#! /usr/bin/gnuplot
#
#

# Styling
set border linewidth 1.5
set pointsize 1.5
set style line 2 lc rgb '#587eB0' pt 7   # circle
set style line 1 lc rgb '#ff60ad' lt 1 lw 2 pt 7 pi -1 ps 1.5

unset key

set terminal latex

