\beamer@endinputifotherversion {3.33pt}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Problem definition}{4}{0}{2}
\beamer@subsectionintoc {2}{1}{Defining the problem}{4}{0}{2}
\beamer@subsectionintoc {2}{2}{Proposal of solutions}{5}{0}{2}
\beamer@subsectionintoc {2}{3}{Linear Programming}{6}{0}{2}
\beamer@subsectionintoc {2}{4}{Metaheuristics}{7}{0}{2}
\beamer@sectionintoc {3}{Results Analysis}{9}{0}{3}
\beamer@sectionintoc {4}{Conclusions}{12}{0}{4}
\beamer@sectionintoc {5}{Bibliography}{14}{0}{5}
