package main

import (
	"./grasp"
	"flag"
	"fmt"
)

const (
	REPORT_TYPE_COMPLETE int = 1
	REPORT_TYPE_SIMPLE   int = 0
)

//ARG DEFINITION CONSTANTS
const (
	ARG_DEFAULT_NUM_ORDERS               int     = 1
	ARG_DEFAULT_NUM_ITERATIONS           int     = 10
	ARG_DEFAULT_ALPHA                    float64 = 0.25
	ARG_DEFAULT_RESULTS_GRAPHIC_CREATION bool    = true
	ARG_DEFAULT_DATA_FILE_NAME           string  = "paper_data.json"
	ARG_DEFAULT_REPORT_TYPE              int     = REPORT_TYPE_COMPLETE
	ARG_NUM_ORDERS                       string  = "n"
	ARG_NUM_ITERATIONS                   string  = "i"
	ARG_ALPHA                            string  = "a"
	ARG_DATA_FILE_NAME                   string  = "f"
	ARG_RESULTS_GRAPHIC_CREATION         string  = "generate_graphics"
	ARG_REPORT_TYPE                      string  = "report_type"
	ARG_USAGE_NUM_ORDERS                 string  = "number of orders"
	ARG_USAGE_NUM_ITERATIONS             string  = "number of iterations performed by the GRASP"
	ARG_USAGE_ALPHA                      string  = "the grade of greediness applied to the solution (used in the RCL)"
	ARG_USAGE_RESULTS_GRAPHIC_CREATION   string  = "defines if it is needed to generate graphics from the results"
	ARG_USAGE_DATA_FILE_NAME             string  = "defines the file from which we want to parse the data (.json file format)"
	ARG_USAGE_REPORT_TYPE                string  = "defines if we want a complete report or not (1=complete, 0=simple)"
)

//COMMANDLINE VAR IN WHICH ARE SAVED ARGS VALUES
var numOrders, numIterations int
var alpha float64
var generateResultsGraphics bool
var inputDataFileName string
var reportType int

func init() {
	//COMMANDLINE ARGS DEFINITION
	flag.IntVar(&numOrders, ARG_NUM_ORDERS, ARG_DEFAULT_NUM_ORDERS, ARG_USAGE_NUM_ORDERS)
	flag.IntVar(&reportType, ARG_REPORT_TYPE, ARG_DEFAULT_REPORT_TYPE, ARG_USAGE_REPORT_TYPE)
	flag.IntVar(&numIterations, ARG_NUM_ITERATIONS, ARG_DEFAULT_NUM_ITERATIONS, ARG_USAGE_NUM_ITERATIONS)
	flag.Float64Var(&alpha, ARG_ALPHA, ARG_DEFAULT_ALPHA, ARG_USAGE_ALPHA)
	flag.BoolVar(&generateResultsGraphics, ARG_RESULTS_GRAPHIC_CREATION, ARG_DEFAULT_RESULTS_GRAPHIC_CREATION, ARG_USAGE_RESULTS_GRAPHIC_CREATION)
	flag.StringVar(&inputDataFileName, ARG_DATA_FILE_NAME, ARG_DEFAULT_DATA_FILE_NAME, ARG_USAGE_DATA_FILE_NAME)
}

/*********************************************
* EXECUTION SAMPLE
*********************************************/
//go run grasp_main.go -n 25 -a 0.2 -i 400

//ARGS *****
//numOrders: 25
//numIterations: 400
//alpha: 0.2
//input_data_file_name: paper_data.json
//report_type: 1
//generate_results_graphics: true

func main() {
	//PARSE COMMANDLINE ARGS
	flag.Parse()
	fmt.Println("ARGS *****\n",
		"//numOrders:", numOrders, "\n",
		"//numIterations:", numIterations, "\n",
		"//alpha:", alpha, "\n",
		"//input_data_file_name:", inputDataFileName, "\n",
		"//report_type:", reportType, "\n",
		"//generate_results_graphics:", generateResultsGraphics, "\n\n")

	var reportOfType grasp.ReportType

	if reportType == REPORT_TYPE_SIMPLE {
		reportOfType = grasp.REPORT_SIMPLE
	} else {
		reportOfType = grasp.REPORT_COMPLETE
	}
	//SETUP CREATION
	graspSetup := grasp.NewSetup(numIterations, numOrders, alpha, inputDataFileName, generateResultsGraphics, reportOfType)
	graspSolver := grasp.NewGraspSolver(graspSetup)
	if err, solution := graspSolver.Solve(); err == nil {
		fmt.Println("SOLUTION FOUND: \n\nscheduling is:")
		solution.PrintScheduling()
		fmt.Println("\nearly sum is: ", solution.GetSumEarly())
		solution.Print()
	} else {
		fmt.Println("SOLUTION NOT FOUND")
	}
}
