// Authors: Eduard Sanou, Alberto Gutiérrez, Jonathan Fragoso

// This package contains the GRASP solver
package grasp

import (
	"encoding/json"
	"io/ioutil"
	"reflect"
	"testing"
	// "sort"
)

func TestGRASPSolver(t *testing.T) {

}

func TestUpdateCandidates(t *testing.T) {
	numUnits := 4
	numOrders := 5
	orders := make([]Order, numUnits*numOrders)

	for i := 0; i < numOrders; i++ {
		for j := 0; j < numUnits; j++ {
			orders[i*numUnits+j].i = i
			orders[i*numUnits+j].j = j
			orders[i*numUnits+j].early = UNBOUNDED_VALUE
		}
	}

	orders = updateCandidates(orders, 1, numUnits)
	expectedSize := (numUnits * numOrders) - numUnits

	if len(orders) != expectedSize {
		t.Errorf("After performing an update, sizes do not match, expected size %v and returned size is: %v", expectedSize, len(orders))
	}

}

func TestReadInputFile(t *testing.T) {
	expected_D_i := []int{
		15, 30, 22, 25, 20, 30, 21, 26, 30, 29, 30, 21, 30, 25, 24, 30, 30, 30, 13, 19, 30, 20, 12, 30, 17}

	expected_S_j := []float64{0.180, 0.175, 0, 0.237}

	expected_Pt_ij := [][]float64{
		[]float64{1.538, 999, 999, 1.194},
		[]float64{1.500, 999, 999, 0.789},
		[]float64{1.607, 999, 999, 0.818},
		[]float64{999, 999, 1.564, 2.143},
		[]float64{999, 999, 0.736, 1.017},
		[]float64{5.263, 999, 999, 3.200},
		[]float64{4.865, 999, 3.025, 3.214},
		[]float64{999, 999, 1.500, 1.440},
		[]float64{999, 999, 1.869, 2.459},
		[]float64{999, 1.282, 999, 999},
		[]float64{999, 3.750, 999, 3.000},
		[]float64{999, 6.796, 7.000, 5.600},
		[]float64{11.250, 999, 999, 6.716},
		[]float64{2.632, 999, 999, 1.527},
		[]float64{5.000, 999, 999, 2.985},
		[]float64{1.250, 999, 999, 0.783},
		[]float64{4.474, 999, 999, 3.036},
		[]float64{999, 1.429, 999, 999},
		[]float64{999, 3.130, 999, 2.687},
		[]float64{2.424, 999, 1.074, 1.600},
		[]float64{7.317, 999, 3.614, 999},
		[]float64{999, 999, 0.864, 999},
		[]float64{999, 999, 3.624, 999},
		[]float64{999, 999, 2.667, 4.000},
		[]float64{5.952, 999, 3.448, 4.902},
	}

	expected_Sd_i_i1_j := [][]float64{
		[]float64{999, 0.3, 0.0, 1.5, 0.6, 0.5, 0.0, 1.1, 0.0, 999, 0.5, 1.0, 0.2, 0.8, 0.7, 0.5, 1.8, 999, 2.5, 0.3, 0.0, 999, 999, 0.0, 0.0},
		[]float64{0.2, 999, 1.3, 0.9, 2.5, 0.2, 0.3, 2.5, 0.4, 999, 0.6, 2.5, 0.5, 0.2, 0.6, 0.0, 1.1, 999, 0.3, 2.5, 0.0, 999, 999, 0.0, 0.0},
		[]float64{0.5, 0.9, 999, 0.5, 0.7, 0.4, 1.5, 0.4, 0.9, 999, 0.2, 1.5, 0.8, 0.0, 0.0, 2.0, 0.6, 999, 0.5, 1.3, 0.0, 999, 999, 0.0, 0.0},
		[]float64{1.1, 0.7, 0.2, 999, 0.8, 2.0, 0.9, 0.0, 1.3, 999, 1.5, 1.0, 1.3, 0.6, 1.3, 0.6, 1.5, 999, 1.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0},
		[]float64{0.5, 1.0, 0.0, 1.3, 999, 0.5, 2.0, 1.3, 0.9, 999, 0.4, 0.3, 2.0, 1.0, 2.0, 0.7, 0.2, 999, 0.3, 0.9, 0.0, 0.0, 0.0, 0.0, 0.0},
		[]float64{0.2, 0.0, 1.3, 1.0, 1.0, 999, 0.7, 1.3, 0.8, 999, 0.7, 0.6, 0.5, 0.7, 0.5, 2.0, 0.9, 999, 1.1, 0.5, 0.0, 999, 999, 0.0, 0.0},
		[]float64{0.9, 0.5, 0.0, 0.0, 1.4, 0.6, 999, 4.0, 0.5, 999, 0.5, 0.8, 0.3, 2.0, 1.1, 0.5, 1.5, 999, 0.9, 1.5, 0.0, 0.0, 0.0, 0.0, 0.0},
		[]float64{1.5, 2.0, 0.4, 1.3, 0.5, 0.0, 0.7, 999, 0.9, 999, 0.4, 1.8, 0.6, 1.5, 0.6, 0.5, 0.7, 999, 0.9, 1.1, 0.0, 0.0, 0.0, 0.0, 0.0},
		[]float64{2.5, 0.6, 0.5, 0.8, 0.6, 1.3, 0.6, 0.2, 999, 999, 2.0, 1.5, 2.0, 0.6, 0.9, 1.3, 1.3, 999, 0.7, 0.3, 0.0, 0.0, 0.0, 0.0, 0.0},
		[]float64{999, 999, 999, 999, 999, 999, 999, 999, 999, 999, 1.0, 1.3, 999, 999, 999, 999, 999, 0.0, 0.8, 999, 999, 999, 999, 999, 999},
		[]float64{0.8, 1.0, 1.3, 0.3, 1.1, 0.4, 2.5, 0.9, 2.0, 0.0, 999, 0.3, 1.0, 2.5, 1.5, 0.6, 0.3, 2.0, 1.3, 0.6, 999, 999, 999, 0.0, 0.0},
		[]float64{0.2, 0.7, 0.6, 0.3, 0.9, 0.3, 0.5, 0.2, 0.4, 0.4, 0.0, 999, 2.0, 1.1, 0.9, 0.2, 2.0, 999, 0.6, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0},
		[]float64{0.9, 0.3, 1.3, 1.1, 1.3, 0.6, 0.4, 1.5, 0.5, 999, 0.4, 1.8, 999, 0.0, 0.0, 0.3, 0.6, 999, 2.5, 1.0, 0.0, 999, 999, 0.0, 0.0},
		[]float64{1.3, 1.5, 2.0, 1.5, 0.4, 2.5, 0.5, 0.0, 1.1, 999, 0.6, 1.5, 0.6, 999, 0.5, 0.5, 0.0, 999, 1.1, 1.5, 0.0, 999, 999, 0.0, 0.0},
		[]float64{1.5, 0.9, 1.3, 0.9, 0.6, 0.1, 0.2, 0.0, 0.3, 999, 1.3, 0.5, 0.4, 0.6, 999, 1.3, 0.0, 999, 1.3, 1.0, 0.0, 999, 999, 0.0, 0.0},
		[]float64{1.3, 2.0, 1.5, 0.5, 0.4, 0.9, 1.3, 0.6, 0.7, 999, 1.5, 2.0, 0.6, 0.4, 0.3, 999, 0.9, 999, 0.5, 0.2, 0.0, 999, 999, 0.0, 0.0},
		[]float64{0.7, 0.7, 0.9, 0.8, 1.4, 0.6, 0.3, 1.0, 0.6, 999, 0.9, 0.4, 0.5, 0.9, 2.0, 0.0, 999, 999, 0.7, 1.1, 0.0, 999, 999, 0.0, 0.0},
		[]float64{999, 999, 999, 999, 999, 999, 999, 999, 999, 0.0, 0.8, 1.3, 999, 999, 999, 999, 999, 999, 1.3, 999, 999, 999, 999, 999, 999},
		[]float64{0.6, 0.5, 1.1, 0.5, 0.4, 1.4, 0.9, 0.4, 0.6, 0.4, 2.5, 0.0, 0.7, 0.7, 0.5, 1.3, 0.7, 0.2, 999, 2.0, 999, 999, 999, 0.0, 0.0},
		[]float64{0.7, 0.5, 2.0, 0.0, 0.0, 1.1, 0.5, 0.6, 1.4, 2.0, 0.4, 0.9, 2.0, 0.3, 0.7, 0.3, 0.5, 999, 0.3, 999, 0.0, 0.0, 0.0, 0.0, 0.0},
		[]float64{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 999, 999, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 999, 999, 0.0, 999, 0.0, 0.0, 0.0, 0.0},
		[]float64{999, 999, 999, 0.0, 0.0, 999, 0.0, 0.0, 0.0, 999, 999, 0.0, 999, 999, 999, 999, 999, 999, 999, 0.0, 2.0, 999, 0.0, 0.0, 0.0},
		[]float64{999, 999, 999, 0.0, 0.0, 999, 0.0, 0.0, 0.0, 999, 999, 0.0, 999, 999, 999, 999, 999, 999, 999, 0.0, 0.0, 0.0, 999, 0.0, 0.0},
		[]float64{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 999, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 999, 0.0, 0.0, 0.0, 0.0, 0.0, 999, 0.0},
		[]float64{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 999, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 999, 0.0, 0.0, 0.0, 2.0, 0.0, 0.0, 999},
	}

	file, err := ioutil.ReadFile("../../data/paper_data.json")

	// Read the input
	if err != nil {
		t.Errorf("file could not be read")
	}

	// parse json data
	var inputData InputData

	if err := json.Unmarshal(file, &inputData); err != nil {
		t.Errorf("error mapping paper_data.json file data into InputData object: %v", err)
	}

	if inputData.NumUnits != 4 {
		t.Errorf("NumUnits should be 4 but is %v instead", inputData.NumUnits)
	}

	if inputData.NumOrders != 25 {
		t.Errorf("NumOrders should be 25 but is %v instead", inputData.NumUnits)
	}

	if !reflect.DeepEqual(inputData.D_i, expected_D_i) {
		t.Errorf("inputData values are different from the expected values %v", inputData.D_i, expected_D_i)
	}

	if !reflect.DeepEqual(inputData.S_j, expected_S_j) {
		t.Errorf("inputData values are different from the expected values %v", inputData.S_j, expected_S_j)
	}

	if !reflect.DeepEqual(inputData.Pt_ij, expected_Pt_ij) {
		t.Errorf("inputData values are different from the expected values %v", inputData.Pt_ij, expected_Pt_ij)
	}

	if !reflect.DeepEqual(inputData.Sd_i_i1_j, expected_Sd_i_i1_j) {
		t.Errorf("inputData values are different from the expected values %v", inputData.Sd_i_i1_j, expected_Sd_i_i1_j)
	}
}

func TestGenerateResultsGraphic(t *testing.T) {

}

// TO RUN BENCHMARKS -> go test -bench . -> executes all benchmarks

func BenchmarkGRASPSolver(b *testing.B) {

}
