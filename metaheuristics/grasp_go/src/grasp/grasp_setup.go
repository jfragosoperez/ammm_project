// Authors: Eduard Sanou, Alberto Gutiérrez, Jonathan Fragoso

// This package contains the GRASP solver
package grasp

import (
	"encoding/json"
	"io/ioutil"
)

type ReportType int

const (
	//generates only graphic of the solution when i=n
	//discarding the analysis of performance when the number of orders
	//increases
	REPORT_SIMPLE ReportType = 1 + iota
	//complete report n executions (from 1 to n)
	//so we can generate report of both the results when i=n
	//and also the performance when the number of orders (n)
	//increases
	REPORT_COMPLETE
)

type GRASPSetup struct {
	// the stopping criterion: number of iterations to perform.
	// the larger the number of iterations, the larger will be the computation
	//time and the better will be the solution found.
	MaxIterations int `json:"max_iterations"`
	// the file name that includes the input
	InputDataFileName string `json:"input_data_file_name"`
	//alpha (greedinessGrade) -> percentage ∈ [0,1]
	Alpha float64 `json:"alpha"`
	//if we want to generate a graphic from the results
	GenerateResultsGraphic bool `json:"generate_results_graphic"`
	//num orders to process
	NumOrders int
	//the report type we need to generate
	ReportType ReportType
}

func (graspSetup GRASPSetup) NeedsToGenerateResultsGraphic() bool {
	return graspSetup.GenerateResultsGraphic
}

func NewSetup(maxIterations, numOrdersToProcess int, alpha float64, inputDataFileName string, generateResultsGraphic bool, reportType ReportType) GRASPSetup {
	return GRASPSetup{
		MaxIterations:     maxIterations,
		InputDataFileName: inputDataFileName,
		Alpha:             alpha,
		GenerateResultsGraphic: generateResultsGraphic,
		NumOrders:              numOrdersToProcess,
		ReportType:             reportType,
	}
}

func (setup GRASPSetup) NeedsToGenerateCompleteReport() bool {
	return setup.ReportType == REPORT_COMPLETE
}

func NewSetupFromFile(setupFileName string) GRASPSetup {

	file, err := ioutil.ReadFile("../setups/" + setupFileName)
	if err != nil {
		panic(err)
	}

	// parse json data
	var setup GRASPSetup

	if err := json.Unmarshal(file, &setup); err != nil {
		panic(err)
	}
	//create setup object from the configuration we've parsed
	return setup
}
