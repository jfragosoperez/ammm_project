// Authors: Eduard Sanou, Alberto Gutiérrez, Jonathan Fragoso

// This package contains the GRASP solver
package grasp

import (
	"testing"
)

func TestGraspSetupFromFile(t *testing.T) {

	expectedGraspSetup := NewSetup(
		1,
		0.20000000000000001,
	)

	graspSetupFromFile := NewSetupFromFile("setup_1.json")

	if graspSetupFromFile.Alpha != expectedGraspSetup.Alpha {
		t.Errorf("alpha when creating a new setup from file is not initialized correctly %v", graspSetupFromFile.Alpha)
	}

	if graspSetupFromFile.InputDataFileName != "paper_data.json" {
		t.Errorf("file name when creating a new setup from file is not initializated correctly %v", graspSetupFromFile.InputDataFileName)
	}

	if graspSetupFromFile.MaxIterations != expectedGraspSetup.MaxIterations {
		t.Errorf("max iterations when creating a new setup from file is not initialized correctly %v", graspSetupFromFile.MaxIterations)
	}

	if !graspSetupFromFile.NeedsToGenerateResultsGraphic() {
		t.Errorf("Our setup was defining that is needed to create a graphic from the results, but when mapping the json into an object, the expected behaviour is not happenning. %v", graspSetupFromFile.GenerateResultsGraphic)
	}
}

func TestNewGraspSetup(t *testing.T) {

	expectedMaxIterations := 30
	expectedAlpha := 0.24
	expectedInputDataFileName := ""

	graspSetup := NewSetup(
		30,
		0.24,
	)

	if graspSetup.Alpha != expectedAlpha {
		t.Errorf("alpha when creating a new setup object is not initialized correctly")
	}

	if graspSetup.MaxIterations != expectedMaxIterations {
		t.Errorf("max iterations when creating a new setup object is not initialized correctly")
	}

	if graspSetup.InputDataFileName != expectedInputDataFileName {
		t.Errorf("input data file name when creating a new setup object is not initialized correctly")
	}
}
