#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "ScheduleDecoder.h"
#include "MTRand.h"
#include "BRKGA.h"
//#include "data.h"

int main(int argc, char* argv[]) {
	unsigned n = n_orders;		// size of chromosomes
	unsigned p = 700;		// size of population
	double pe = 0.20;		// fraction of population to be the elite-set
	double pm = 0.10;		// fraction of population to be replaced by mutants
	double rhoe = 0.70;		// probability that offspring inherit an allele from elite parent
	unsigned K = 2;			// number of independent populations
	unsigned MAXT = 8;		// number of threads for parallel decoding
	unsigned X_INTVL = 8;		// exchange best individuals at every 100 generations
	unsigned X_NUMBER = 2;		// exchange top 2 best
	unsigned MAX_GENS = 20;		// run for 1000 gens
	long unsigned rngSeed = 0;	// seed to the random number generator
	
	int c;
	
	opterr = 0;

	while ((c = getopt (argc, argv, "n:p:e:m:r:k:t:x:b:g:s:")) != -1) {
		switch(c) {
		case 'n':
			n = atoi(optarg);
			break;
		case 'p':
			p = atoi(optarg);
			break;
		case 'k':
			K = atoi(optarg);
			break;
		case 't':
			MAXT = atoi(optarg);
			break;
		case 'x':
			X_INTVL = atoi(optarg);
			break;
		case 'b':
			X_NUMBER = atoi(optarg);
			break;
		case 'g':
			MAX_GENS = atoi(optarg);
			break;
		case 's':
			rngSeed = atoi(optarg);
			break;
		case 'e':
			pe = atof(optarg);
			break;
		case 'm':
			pm = atof(optarg);
			break;
		case 'r':
			rhoe = atof(optarg);
			break;
		case '?':
			printf("Error parsing cli input parameters\n");
			return 1;
		default:
			abort();
		}
	}

	printf("Number of orders: %d\n", n);
	printf("Starting BRGKA with:\n");
	printf("Chromosome size: %d\n", n);
	printf("Population size: %d\n", p);
	printf("Elite-set fraction: %f\n", pe);
	printf("Mutants fraction: %f\n", pm);
	printf("Prob. of inheriting allele from elite parent: %f\n", rhoe);
	printf("Num. of indep. populations: %d\n", K);
	printf("Num. of threads: %d\n", MAXT);
	printf("Num. of generations to exchange best individuals: %d\n", X_INTVL);
	printf("Num. of best to exchange: %d\n", X_NUMBER);
	printf("Num. generations: %d\n", MAX_GENS);
	printf("Random seed: %lu\n", rngSeed);
	printf("\n");

	ScheduleDecoder decoder;			// initialize the decoder
	
	MTRand rng(rngSeed);				// initialize the random number generator
	
	// initialize the BRKGA-based heuristic
	BRKGA< ScheduleDecoder, MTRand > algorithm(n, p, pe, pm, rhoe, decoder, rng, K, MAXT);
	
	unsigned generation = 0;		// current generation
	do {
		algorithm.evolve();	// evolve the population for one generation
		
		if((++generation) % X_INTVL == 0) {
			algorithm.exchangeElite(X_NUMBER);	// exchange top individuals
		}
	} while (generation < MAX_GENS);
	
	std::cout << "Best solution found has objective value = "
	 	  << algorithm.getBestFitness() << std::endl;
	decoder.print_chromosome(algorithm.getBestChromosome());
	//std::cout << "Best chromosome found is : \n"
	//	  << algorithm.getBestChromosome() << std::endl;
	
	return 0;
}
