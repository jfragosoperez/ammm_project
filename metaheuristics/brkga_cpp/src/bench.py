#! /usr/bin/env python3

import subprocess, re, sys, time

print("Benchmarking BRKGA solution")


bin = "./brkga_scheduler"

command = []
command.append(bin)

if len(sys.argv) > 1:
    flags = sys.argv[1]
    print("flags:", flags)
    command += flags.split(" ")
print()

results = []

for i in range(1, 26):

    start = time.time()
    p = subprocess.Popen(command + ["-n" + str(i)], stdout=subprocess.PIPE)
    out, err = p.communicate()
    end = time.time()
    #print(out)
    res = re.findall("(?<=\(sum\(C_i\)\)\: )[0-9.]*", out.decode("ASCII"))
    if len(res) != 1:
        print("Error at output:")
        print(out.decode("ASCII"))
        exit(1)
    results.append((i, float(res[0]), (end-start)*1000))
    #print(float(res[0]))

for r in results:
    print(r[0], r[1], r[2])
