#include <iostream>
#include "ScheduleDecoder.h"
#include "MTRand.h"
#include "BRKGA.h"
//#include "data.h"

int main(int argc, char* argv[]) {
	/*
	std::vector<double> chromosome(n_orders);
	for (int i = 0; i < n_orders; i++) {
		chromosome[i] = i + 1;
	}
	*/
	//std::vector<double> chromosome {0.7628,0.3096,0.6545,0.5935,0.8921,0.3224,0.8322,0.3828,0.4065,0.2115,0.7416,0.6708,0.3631,0.4678,0.5640,0.1496,0.0455,0.1770,0.9678,0.9812};
	std::vector<double> chromosome {0.4562,0.5684,0.0188,0.6176,0.6121,0.6169,0.9437,0.6818};
	
	double res = 0;
	
	ScheduleDecoder dec = ScheduleDecoder();
	res = dec.decode(chromosome);
	std::cout << "Result: " << res << std::endl;
	dec.print_chromosome(chromosome);
	
	return 0;
}
