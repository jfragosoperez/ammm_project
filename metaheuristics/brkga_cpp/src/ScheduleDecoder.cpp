/*
 * ScheduleDecoder.cpp
 *
 *  Created on: 5/12/2014
 *      Authors: Alberto Gutierrez, Jonathan Fragoso, Eduard Sanou
 */

#include <float.h>
#include <string.h>
#include <stdio.h>

#include <array>

//#include "data.h"
#include "ScheduleDecoder.h"

bool verbose = 0;
unsigned int n_orders_todo;

ScheduleDecoder::ScheduleDecoder() { }

ScheduleDecoder::~ScheduleDecoder() { }

bool order_comp(order *a, order *b) {
	return a->finish < b->finish;
}

void ScheduleDecoder::check(order *orders) {
	// Initialize the array of units to store the solution
	std::array<std::vector<order*>, n_units> units; // default construction
	std::fill(units.begin(), units.end(), std::vector<order*>());

	for (int i = 0; i < n_orders_todo; i++) {
		units[orders[i].j].push_back(&orders[i]);
	}	
	for (int j = 0; j < n_units; j++) {
		std::sort(units[j].begin(), units[j].end(), order_comp);
	}


	int i1, i2;
	double dur;
	double e = 0.001;
	for (int j = 0; j < n_units; j++) {
		printf("Unit: %d\n", j);
		if (units[j].size() == 0) {
			continue;
		}
		i1 = units[j][0]->i;
		if (orders[i1].start < 0) {
			printf("Error, order %d starts before 0\n", orders[i1].i);
		}
		for (int i = 1; i < units[j].size(); i++) {
			i2 = units[j][i]->i;
			dur = su_j[j] + pt_ij[i1][j];
			if (orders[i1].start + dur + sd_ii1j[i1][i2] > 
					orders[i2].start + e) {
				printf("Error, order %d and %d overlap\n", 
						i1, i2);
				printf("%f > %f\n", orders[i1].start + dur + sd_ii1j[i1][i2], orders[i2].start);
			}
			if ((orders[i1].start + dur) > (D_i[i1] + e)) {
				printf("Error, order %d doesn't fulfill deadline\n",
						orders[i1].i);
			}
			i1 = i2;
		}
		printf("\n");
	}
}

void ScheduleDecoder::print_chromosome(const std::vector<double> &chromosome) {

	double sum_c = 0;
	// Initialize the structs for all the orders
	order orders[n_orders_todo];
	for (unsigned int i = 0; i < n_orders_todo; i++) {
		orders[i].i = i;
		// When unit is -1, the order has not been assigned yet.
		orders[i].j = -1;
		orders[i].early = 9999;
	}

	decode_chromosome(chromosome, orders);

	printf("Chromosome:\n{");
	for (int i = 0; i < n_orders_todo - 1; i++) {
		printf("%.4f,", chromosome[i]);
	}
	printf("%.4f}\n\n", chromosome[n_orders_todo - 1]);

	printf("Solution:\n");
	printf("order\tunit\tCi\tEi\n");
	for (int i = 0; i < n_orders_todo; i++) {
		sum_c += orders[i].finish;
		printf("i%d\tJ%d\t%.2f\t%.2f\n",
			i+1, orders[i].j+1, orders[i].finish, orders[i].early);
	}
	printf("\nObjective function (sum(C_i)): %.4f\n", sum_c);

	check(orders);
}

order best_solution(unsigned int i, unsigned int j, 
		    std::array<std::vector<order*>, n_units> *units) {


	order prev_o;
	double dur, prev_limit;
	double finish_limit;
	order tmp_order;
	std::vector<order*> *u;
	auto prev_o_it = (*units)[0].rbegin();

	tmp_order.i = i;
	tmp_order.j = j;
	tmp_order.early = 99999;

	if (pt_ij[i][j] > 900) {
		return tmp_order;
	}
	if (verbose)
		printf("-Try unit %d\n", j);
	u = &(*units)[j];
	// Find at which assigned order we reach D_i
	for (prev_o_it = u->rbegin(); prev_o_it != u->rend(); prev_o_it++) {
		if ((*prev_o_it)->finish < D_i[i]) {
			break;
		}
	}
	// Check what's the limit time to finish the order
	if (prev_o_it != u->rbegin()) {
		prev_limit = (*(prev_o_it-1))->start - 
			       sd_ii1j[i][(*(prev_o_it-1))->i];
		finish_limit = D_i[i] < prev_limit ? D_i[i] : prev_limit;
	} else {
		finish_limit = D_i[i];
	}
	if (prev_o_it == u->rend()) {
		dur = pt_ij[i][j] + su_j[j];
		//if (dur <= D_i[i] && (dur + sd_ii1j[i][(*(prev_o_it-1))->i]) < (*(prev_o_it-1))->start) {
		if (dur <= finish_limit) {
			// fit_beginnning!!!;
			// check if best -> update
			tmp_order.finish = finish_limit;
			tmp_order.start = tmp_order.finish - 
				          (pt_ij[i][j] + su_j[j]);
			tmp_order.early = D_i[i] - tmp_order.finish;
			if (verbose)
				printf("[No orders previously] Try assign order %d to unit %d before D\n",
					tmp_order.i, tmp_order.j);
			
		}
		return tmp_order;
	}
	// Find if i fits between the prev_o_it and D_i
	dur = sd_ii1j[(*prev_o_it)->i][i] + pt_ij[i][j] + su_j[j];
	

	//Check if the order fits between bounds
	if ((*prev_o_it)->finish + dur <= finish_limit) {
		// Create (i][ j) solution.
		tmp_order.finish = finish_limit;
		tmp_order.start = tmp_order.finish - (pt_ij[i][ j] + su_j[j]);
		tmp_order.early = D_i[i] - tmp_order.finish;
		if (verbose)
			printf("[Order previously] Try assign order %d to unit %d before D\n",
				tmp_order.i, tmp_order.j);
		return tmp_order;
	} else {
		prev_o_it++;
	}
	// Find after which o1 we can fit i
	for (; prev_o_it != u->rend(); prev_o_it++) {
		dur = sd_ii1j[(*prev_o_it)->i][i] + pt_ij[i][ j] + su_j[j];
		finish_limit = (*(prev_o_it-1))->start - sd_ii1j[i][(*(prev_o_it-1))->i];

		// Check if the order fits between bounds
		if ((*prev_o_it)->finish + dur <= finish_limit) {
			// Create (i][ j) solution.
			tmp_order.finish = finish_limit;
			tmp_order.start = tmp_order.finish - (pt_ij[i][ j] + su_j[j]);
			tmp_order.early = D_i[i] - tmp_order.finish;
			break;
		}
	}
	// We reached the beginning and
	if (prev_o_it != u->rend()) {
		return tmp_order;
	} else { 
		dur = pt_ij[i][ j] + su_j[j];
		finish_limit = (*(prev_o_it-1))->start - sd_ii1j[i][(*(prev_o_it-1))->i];
		if (dur <= finish_limit) {
			tmp_order.finish = finish_limit;
			tmp_order.start = tmp_order.finish - (pt_ij[i][ j] + su_j[j]);
			tmp_order.early = D_i[i] - tmp_order.finish;
			return tmp_order;
		}
	}
	return tmp_order;
}

void ScheduleDecoder::decode_chromosome(const std::vector<double> &chromosome,
		order *orders) const {

	std::vector< std::pair<double, unsigned> > ranking(chromosome.size());
	for(unsigned i = 0; i < chromosome.size(); ++i) {
		ranking[i] = std::pair< double, unsigned >(chromosome[i], i);
	}

	// Here we sort 'permutation', which will then produce a permutation 
	// of [n] in pair::second:
	std::sort(ranking.begin(), ranking.end());

	// Initialize the array of units to store the solution
	std::array<std::vector<order*>, n_units> units; // default construction
	std::fill(units.begin(), units.end(), std::vector<order*>());
	
	
	unsigned int i;
	order *o;
	order tmp_order;
	
	//o->early = 9999;
	
	// For each i find in which unit the earliness is minimum, and assign
	// it there.
	for(std::vector< std::pair<double, unsigned> >::const_iterator it_i =
		ranking.begin();it_i != ranking.end(); it_i++) {
		// i holds the order index
		i = it_i->second;
		o = &orders[i];
		if (verbose)
			printf("Trying to fit order %d into solution\n", i);
		// Find best available start time for that j
		for (int j = 0; j < n_units; j++) {
			tmp_order = best_solution(i, j, &units);
			if (tmp_order.early < o->early) {
				memcpy(o, &tmp_order, sizeof(*o));
			}
		}
		if (verbose)
			printf("Order %d goes to unit %d, start=%f finish=%f early=%f\n\n", 
				tmp_order.i, tmp_order.j, tmp_order.start, tmp_order.finish,
				tmp_order.early);
		// It's over 9000!!!
		if (o->j == -1) {
			return;
		}
		// Add an order:
		units[o->j].push_back(o);
		std::sort(units[o->j].begin(), units[o->j].end(), order_comp);
	}
	return;
}


// Runs in \Theta(n \log n):
double ScheduleDecoder::decode(const std::vector<double> &chromosome) const {

	n_orders_todo = chromosome.size();
	// Initialize the structs for all the orders
	order orders[n_orders_todo];
	for (unsigned int i = 0; i < n_orders_todo; i++) {
		orders[i].i = i;
		// When unit is -1, the order has not been assigned yet.
		orders[i].j = -1;
		orders[i].early = 9999;
	}

	decode_chromosome(chromosome, orders);
	
	double sum_early = 0;
	for (int i = 0; i < n_orders_todo; i++) {
		if (orders[i].j == -1) {
			return DBL_MAX;
		}
		sum_early += orders[i].early;
	}
	return sum_early;
}
